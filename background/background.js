const defaultConfig = {
    token: "none"
}
function loadData() {
    browser.storage.local.get("betterCrunchyPlayer").then((res) => {
        if (res.betterCrunchyPlayer === undefined) {
            saveDefault()
        }
    });
}

function saveDefault(){
    browser.storage.local.set({
        "betterCrunchyPlayer": defaultConfig
    });
}

loadData();