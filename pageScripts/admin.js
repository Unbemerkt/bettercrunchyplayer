var duration;

function startAdmin() {
    console.log("Starting admin service...")
    checkPlayerAdminReady();
}

function checkPlayerAdminReady() {

    if (document.getElementById("vilos-player") === null||!VILOS_PLAYERJS) {
        setTimeout(checkPlayerAdminReady, 500);
        return;
    }
        addStyle();

}

function addStyle() {
    const styleTag = document.createElement("style");
    styleTag.id = "better-crunchy-player-admin-style"
    styleTag.innerHTML = "#betterCrunchyPlayer-adminContainer{border:1px solid #000; padding: 1em;}#betterCrunchyPlayer-adminContainer a{cursor:pointer;}"
    document.getElementsByTagName("head")[0].appendChild(styleTag);
    const linkTag = document.createElement("link");
    linkTag.href="https://fonts.googleapis.com/icon?family=Material+Icons";
    linkTag.rel = "stylesheet";
    document.getElementsByTagName("head")[0].appendChild(linkTag);
    createContainer();
}

var container;

function createContainer() {
    var frame = document.getElementById("message_box");
    container = document.createElement("div");
    container.id = "betterCrunchyPlayer-adminContainer";
    document.getElementById("message_box").after(container, frame);

    updateContent();
    messageListener();
}

const STATES = [
    "INTRO",
    "REVIEW",
    "OUTRO",
    "NEXT_EPISODE"
]

function updateContent() {
    if (container === undefined) return;
    if (betterCrunchyVideoData.TYPE !== undefined) {
        container.innerHTML = "<h2>BetterCrunchyPlayer - Admin</h2>Video is not in DataBase! <a onclick='createVideo()'>Create</a>"
        return;
    }
    startTimer()
    var html = "";
    html+="<a style='float: right' onclick='closAdminContainer()'>X</a>"
    html += "<h4>Create State:</h4>"
    html += "<i id='bcp-time'>Loading...</i>  "
    html += "<i id='bcp-dtime'>Loading...</i>"
    html += "<div>";
    html += "<select id='bcp-cstate-state'>";
    STATES.forEach(state => {
        html += "<option value='" + state + "'>" + state + "</option>";
    });
    html += "";
    html += "</select>";
    html += "<br>";
    html += "<input type='number' id='bcp-cstate-start' step='0.1' min='0' placeholder='Start'>"
    html += "<br>";
    html += "<a onclick='copyTime(\"START\")'>Insert Current Time</a>";
    html += "<br>";
    html += "<br>";
    html += "<input type='number' id='bcp-cstate-end' step='0.1' min='0' placeholder='End'>"
    html += "<br>";
    html += "<a onclick='copyTime(\"START-END\")'>Insert Current Time</a>";
    html += "<br>";
    html += "<a onclick='copyTime(\"END-END\")'>Insert Duration Time</a>";
    html += "</div>";
    html += "<br>";
    html += "<a onclick='createState()'>Create State</a>"
    html += "<hr>";
    html += "<h4>Existing States:</h4>";
    html += "<br>";
    if (betterCrunchyVideoData.states.length >= 0) {
        betterCrunchyVideoData.states.forEach(state => {
            html += "<div>";
            html += "<i>" + state.STATE + "</i>";
            html += "";
            html += "<input type='number' value='" + state.start + "' disabled>"
            html += "<input type='number' value='" + state.end + "' disabled >"
            html += ""
            html += "<a onclick='deleteState(" + betterCrunchyVideoData.states.indexOf(state) + ")'>delete</a>"
            html += "</div>";
        })
        if (!isHandlerStarted)html += "<br><a onclick='startAdminHandler()'>Start Player-Handler</a>"
        html += "<br><a id='bcp_saveBTN' onclick='saveVideoData()'>Save</a><span class=\"material-icons\" id='saveCheck' style='color: #00ff00; display: none'>done</span>"
    }

    container.innerHTML = html;
}
function copyTime(type){
    if (type==="START"){
        document.getElementById("bcp-cstate-start").value = lastTime;
    }else if (type==="START-END"){
        document.getElementById("bcp-cstate-end").value = lastTime;
    } else if (type==="END-END"){
        document.getElementById("bcp-cstate-end").value = duration;
    }
}
function deleteState(index) {
    if (index > -1) {
        betterCrunchyVideoData.states.splice(index, 1);
    }
    updateContent();
}

function saveVideoData() {
    document.getElementById("bcp_saveBTN").setAttribute("style","color: grey;");
    window.postMessage({TYPE: "BCP_SAVE_VIDEO", VIDEO_DATA: betterCrunchyVideoData})
}

function createState() {
    const start = $("#bcp-cstate-start").val();
    const end = $("#bcp-cstate-end").val();
    const state = $("#bcp-cstate-state").val();
    if (start === undefined || end === undefined || state === undefined || start === end) {
        return;
    }
    betterCrunchyVideoData.states.push({
        "STATE": state,
        start,
        end
    })
    updateContent();
}

var isRunning = false;

function startTimer() {
    updateDuration();
    if (isRunning) return;
    isRunning = true;
    updateCurrentTime();
}

var lastTime;
function updateDuration(){
    VILOS_PLAYERJS.getDuration((dat) => {
        if (dat===undefined){
            setTimeout(updateDuration,1000);
            return;
        }
        duration = dat;
        document.getElementById("bcp-dtime").innerHTML="Duration: "+dat
    })
}
function updateCurrentTime() {
    if (!isRunning) return;
    const time = document.getElementById("bcp-time");
    VILOS_PLAYERJS.getCurrentTime((data) => {
        if (time !== null && lastTime !== data) {
            time.innerHTML = "Current Time: " + data;
            lastTime = data;
        }
        setTimeout(updateCurrentTime, 1000);
    });
}

function createVideo() {
    betterCrunchyVideoData = {
        "states": []
    }
    updateContent();
}

function closAdminContainer() {
    container.remove();
    isRunning = false;
}

function messageListener() {
    window.addEventListener("message", (event) => {
        if (event.data.TYPE && (event.data.TYPE === "BCP_SAVE_VIDEO_COMPLETE")) {
            document.getElementById("bcp_saveBTN").setAttribute("style","");
            document.getElementById("saveCheck").setAttribute("style","color: #00ff00; font-size: 12px;");

        }
    })
}

startAdmin();