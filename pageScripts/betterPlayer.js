function start() {
    console.log("Starting BetterCrunchyPlayer-Handler")
    addStyle()
}

var frame;
var button;

function addButton() {
    console.info("Adding Button")
    button = document.createElement("button");
    button.innerHTML = "Button"
    frame.appendChild(button);
    startHandler();
}

var isShown = true;

function hideButton() {
    if (!isShown) return
    isShown = false;
    button.setAttribute("style", "display: none");
}

function showButton() {
    if (isShown) return;
    isShown = true;
    button.setAttribute("style", "");
}

const buttonSkipStates = {
    INTRO: {
        TEXT: "Skip Intro"
    },
    REVIEW: {
        TEXT: "Skip Review"
    },
    OUTRO: {
        TEXT: "Skip Outro"
    },
    NEXT_EPISODE:{
        TEXT: "Next Episode"
    }
}

function setButton(text, time) {
    button.innerHTML = text;
    button.setAttribute("onclick", "seekTo(" + time + ")")
}

function addStyle() {
    console.info("Adding Style")
    const styleTag = document.createElement("style");
    styleTag.id = "better-crunchy-player-style"
    styleTag.innerHTML = ".better-crunchy-player-parent > button {opacity: 0.8; position:relative;float:right;right:10px;" +
        "bottom:150px;transition:0.5s;border:none;background-color: #ffffff; border-radius: 5px; padding: 5px; cursor: pointer}" +
        ".better-crunchy-player-parent > button:hover {opacity: 1;}"
    document.getElementsByTagName("head")[0].appendChild(styleTag);
    frame = document.getElementById("showmedia_video_player");
    frame.setAttribute("class", frame.getAttribute("class") + " better-crunchy-player-parent");
    checkPlayerReady();

}

function checkPlayerReady() {
    if (document.getElementById("vilos-player") === null) {
        setTimeout(checkPlayerReady, 500);
    } else {
        addButton();
    }

}

function getVideoId() {
    const url = window.location.toString();
    const splittedUrl = url.split("/")
    const tmp = splittedUrl[splittedUrl.length - 1];
    if (tmp.includes("?")) {
        return tmp.split("?")[0];
    } else {
        return tmp;
    }
}

// const betterCrunchyVideoData = { //example
//     "states": [
//         {
//             "start": 138.0,
//             "end": 220.0,
//             "STATE": "INTRO"
//         }
//     ],
//     "duration": undefined
// };
var isHandlerStarted = false;
function startAdminHandler(){
    if (isHandlerStarted)return;
    startHandler();
    updateContent()
}
function startHandler() {
    isHandlerStarted = true;
    if (betterCrunchyVideoData.TYPE !== undefined) {
        console.error("Cloud not start BetterCrunchyPlayer-Handler!", betterCrunchyVideoData.DATA)
        isHandlerStarted = false;
        hideButton();
        return;
    }

    hideButton();
    console.info("Starting handler")
    try {
        VILOS_PLAYERJS.getDuration((dat) => {
            if (dat === undefined) {
                setTimeout(startHandler, 500)
                return;
            }
            betterCrunchyVideoData.duration = dat;
            handle();
        })
    } catch (e) {
        console.error(e);
    }
}

function handle() {

    VILOS_PLAYERJS.getCurrentTime((data) => {
        var state = getCurrentState(parseFloat(data.toString()));
        if (state === undefined) {
            hideButton();
            setTimeout(handle, 1000)
            return;
        }
        setButton(buttonSkipStates[state.STATE].TEXT, state.end);
        showButton();
        setTimeout(handle, 1000)

    });

}

function seekTo(time) {
    VILOS_PLAYERJS.setCurrentTime(time, (data) => {
    })
}


function getCurrentState(time) {
    if (betterCrunchyVideoData.duration === undefined) {
        return undefined;
    }
    for (let i = 0; i < betterCrunchyVideoData.states.length; i++) {
        const state = betterCrunchyVideoData.states[i];
        if (state !== undefined && (time >= state.start && time <= state.end)) {
            return state;
        }

    }
    return undefined;
}

start();