//https://extensionworkshop.com/documentation/publish/submitting-an-add-on/
//VILOS_PLAYERJS.getCurrentTime((dat)=>{console.log(dat)})
//https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json/web_accessible_resources
// const serverURL = "http://localhost:3001/"
const serverURL = "https://bcp.hostings4u.eu/"
var savedData = {};

function init() {
    browser.storage.local.get("betterCrunchyPlayer").then((res) => {
        savedData = res.betterCrunchyPlayer;
        res.loggedId = false;
        injectScript();
        messageListener();
    })

}

function messageListener() {
    window.addEventListener("message", (event) => {
        if (event.data.TYPE && (event.data.TYPE === "BCP_SAVE_VIDEO")) {
            var videoData = event.data.VIDEO_DATA;
            if (savedData.token===undefined){
                return;
            }
            sendPost(serverURL + "update", {videoID: getVideoId(), videoData, token: savedData.token}).then(r => {
                window.postMessage({TYPE: "BCP_SAVE_VIDEO_COMPLETE"})
            })
        }
    })
}

function getVideoId() {
    const url = window.location.toString();
    const splittedUrl = url.split("/")
    const tmp = splittedUrl[splittedUrl.length - 1];
    if (tmp.includes("?")) {
        return tmp.split("?")[0];
    } else {
        return tmp;
    }
}


function injectScript() {
    sendPost(serverURL, {
        videoID: getVideoId()
    }).then((data) => {
        const dataScriptElem = document.createElement("script");
        if (data === undefined || data === "") {
            console.error("[BetterCrunchyPlayer] Received invalid data from Server!")
            return;
        }
        if (data.TYPE !== undefined) {
            data.token = savedData.token;
            dataScriptElem.innerHTML = "var betterCrunchyVideoData = " + JSON.stringify(data);
            document.getElementsByTagName("body")[0].appendChild(dataScriptElem);
        } else {
            data.token = savedData.token;
            dataScriptElem.innerHTML = "var betterCrunchyVideoData = " + JSON.stringify(JSON.parse(data.data));
            document.getElementsByTagName("body")[0].appendChild(dataScriptElem);
        }

        injectPlayerScript();
        injectAdminScript();

    })
}

function injectPlayerScript() {
    const scriptElm = document.createElement("script");
    scriptElm.src = chrome.runtime.getURL('pageScripts/betterPlayer.js');
    // scriptElm.innerHTML="function getPlayer(){return VILOS_PLAYERJS;}"
    document.getElementsByTagName("body")[0].appendChild(scriptElm);
}

function injectAdminScript() {
    //create admin interface
    authToken(() => {
        console.log("")
        console.log("")
        console.log("===================================================================")
        console.log("[BetterCrunchyPlayer] Press Shift + Ctrl + X to open Login window")
        console.log("===================================================================")
        console.log("")
        console.log("")
        shortcut.add("shift+ctrl+x", () => {
            openAuth()
        })
    }, () => {
        const adminScript = document.createElement("script");
        adminScript.src = chrome.runtime.getURL('pageScripts/admin.js');
        document.getElementsByTagName("body")[0].appendChild(adminScript);
    })

}

function openAuth() {
    var div = document.createElement("div");
    div.setAttribute("style", "z-index: 999999; position: absolute; padding: 1em; background: #ffffff; top: 0; left: 0;")
    div.id = "BCPL_div";

    var btn = document.createElement("a");
    btn.innerHTML = "x";
    btn.setAttribute("onclick", 'document.getElementById("BCPL_div").remove()')

    var h = document.createElement("h4");
    h.innerHTML = "BetterCrunchyLogin - Login"

    var inputName = document.createElement("input");
    inputName.id = "BCPL_name";
    inputName.placeholder = "Username"
    inputName.setAttribute("type", "text");

    var inputPW = document.createElement("input");
    inputPW.id = "BCPL_pw";
    inputPW.placeholder = "Password"
    inputPW.setAttribute("type", "password");

    var lognBtn = document.createElement("button");
    lognBtn.innerHTML = "Login";
    inputPW.setAttribute("type", "password");
    lognBtn.addEventListener('click', () => {
        auth()
    })
    div.appendChild(btn)
    div.appendChild(h)
    div.appendChild(inputName);
    div.appendChild(document.createElement("br"));
    div.appendChild(inputPW);
    div.appendChild(document.createElement("br"));
    div.appendChild(lognBtn);
    document.getElementsByTagName("body")[0].appendChild(div);
}

function auth() {

    const name = document.getElementById("BCPL_name").value;
    var pw = document.getElementById("BCPL_pw").value;
    if (pw === "" || name === "") return;
    pw = md5(pw);
    document.getElementById("BCPL_div").remove();
    sendPost(serverURL + "auth", {name, pw}).then((data) => {
        if (data.TYPE === "OK") {
            savedData.token = data.token;
            browser.storage.local.set({
                "betterCrunchyPlayer": {token: data.token}
            });
            const adminScript = document.createElement("script");
            adminScript.src = chrome.runtime.getURL('pageScripts/admin.js');
            document.getElementsByTagName("body")[0].appendChild(adminScript);
        } else {
            openAuth()
        }

    })
}

function authToken(cb, cb2) {
    if (savedData.token === undefined || savedData.token === "none") {
        cb();
        return;
    }
    sendPost(serverURL + "authToken", {
        token: savedData.token
    }).then((data) => {
        if (data.TYPE === "ERROR") {
            savedData.loggedId = false;
            cb()
        } else if (data.TYPE === "OK") {
            savedData.loggedId = true;
            browser.storage.local.set({
                "betterCrunchyPlayer": {token: data.token}
            });
            cb2(data.token);
        }
    })
}

async function sendPost(url, data) {
    // Default options are marked with *
    const response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        // mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        // credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    return response.json(); // parses JSON response into native JavaScript objects
}


init();


